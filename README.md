# Add ticket git hook

![coverage](https://gitlab.com/matthewhughes/add-ticket-git-hook/badges/main/coverage.svg)
![pipeline-status](https://gitlab.com/matthewhughes/add-ticket-git-hook/badges/main/pipeline.svg)

A [`prepare-commit-msg`](https://git-scm.com/docs/githooks#_prepare_commit_msg)
hook mean to be run through [`pre-commit`](https://pre-commit.com/) to read the
ticket from the current branch name and add it to the commit body as a `Ticket:`
trailer. Sample usage:

``` console
$ git checkout -b 'FEAT-123-add-the-feature'
# imagine some work is done
$ git commit --message "Add the feature"
$ git log -1
Author: John Smith <john@example.com>
Date:   Sun May 7 11:23:34 2023 +0100

    Add the feature
    
    Ticket: FEAT-123
```

## Installation

Install `pre-commit` per [the instructions](https://pre-commit.com/#install)
then add an entry in `.pre-commit-config.yaml`:

``` yaml
repos:
-   repo: https://gitlab.com/matthewhughes/add-ticket-git-hook
    rev: v0.1.0
    hooks:
    -   id: add-ticket-to-commit
```

And run:

``` console
$ pre-commit install --hook-type prepare-commit-msg
```

## Ticket detection

The ticket is detected by taking the trailing part of the branch name following
any `'/'` character and matching it against the regular expression
`"^[A-Z]+-[0-9]+"`, for example each of the following the ticket as `FEAT-123`:

  - `FEAT-123-add-the-feature`
  - `feature-name/FEAT-123-add-the-feature`
  - `dev-name/feature-name/FEAT-123-add-the-feature`
