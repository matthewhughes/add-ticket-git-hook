from __future__ import annotations

import os
import re
import subprocess
import sys
from collections.abc import Sequence

TICKET_PREFIX = "Ticket: "
TICKET_RE = re.compile("^[A-Z]+-[0-9]+")

# from https://git-scm.com/docs/githooks can be:
#   message (if a -m or -F option was given)
#   template (if a -t option was given or the configuration option commit.template is set)
#   merge (if the commit is a merge or a .git/MERGE_MSG file exists)
#   squash (if a .git/SQUASH_MSG file exists)
#   or commit, followed by a commit object name (if a -c, -C or --amend option was given).
# I don't have much need to add tickets to merge commits, so that is ignored
SUPPORTED_SOURCES = (None, "message", "template", "commit")


def main(argv: Sequence[str] | None = None) -> int:
    if argv is None:  # pragma: no cover
        argv = sys.argv[1:]

    # see https://pre-commit.com/#prepare-commit-msg
    commit_msg_file = argv[0]
    commit_msg_src = os.environ.get("PRE_COMMIT_COMMIT_MSG_SOURCE")

    if commit_msg_src not in SUPPORTED_SOURCES:
        return 0

    branch = _get_branch()
    if branch is None:
        return 0

    ticket = _get_ticket_from_branch(branch)
    if ticket is None:
        return 0

    with open(commit_msg_file) as f:
        commit_content = f.read()

    out_content = _add_ticket_to_commit(ticket, commit_msg_src, commit_content)
    if out_content is not None:
        with open(commit_msg_file, "w") as f:
            f.write(out_content)

    return 0


def _add_ticket_to_commit(
    ticket: str, commit_source: str | None, commit_content: str
) -> str | None:
    ticket_string = TICKET_PREFIX + ticket

    # no-op if the ticket details already exist
    if ticket_string in commit_content:
        return None

    return _add_ticket_details(commit_content, ticket_string, commit_source)


def _add_ticket_details(
    commit_content: str, ticket_string: str, commit_source: str | None
) -> str:
    comment_start = re.search("^#", commit_content, flags=re.MULTILINE)
    if comment_start is not None:
        # commit open with editor: contents are the commit followed by
        # auto-generated comments
        # add the ticket between the end of the contents and the start of the comments
        comment_location = comment_start.span(0)[0]
        commit_text = commit_content[:comment_location]
        commit_comments = commit_content[comment_location:]

        ticket_string = f"\n{ticket_string}\n"
        if commit_source is None:
            ticket_string += "\n"

        return commit_text + ticket_string + commit_comments
    else:
        # commit not open with editor, just contains contents
        # append the ticket details
        return commit_content + f"\n{ticket_string}\n"


def _get_branch() -> str | None:
    cmd = ("git", "rev-parse", "--abbrev-ref", "HEAD")
    try:
        proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
    except OSError as e:
        print(f"Failed to run `f{' '.join(cmd)}`: {e}", file=sys.stderr)
        return None

    stdout, stderr = proc.communicate()
    stdout_text = stdout.decode() if stdout is not None else ""
    stderr_text = stderr.decode() if stderr is not None else ""

    if proc.returncode != 0:
        print(
            f"`{' '.join(cmd)}` failed: {stdout_text}, {stderr_text}", file=sys.stderr
        )
        return None
    else:
        return stdout_text


def _get_ticket_from_branch(branch: str) -> str | None:
    """ """
    branch_details = branch.rsplit("/", 1)[-1]

    ticket_match = TICKET_RE.search(branch_details)
    if ticket_match is not None:
        return ticket_match.group()
    else:
        return None


if __name__ == "__main__":
    raise sys.exit(main())
