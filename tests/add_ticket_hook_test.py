from __future__ import annotations

import subprocess
import tempfile
from collections.abc import Generator
from contextlib import contextmanager
from unittest.mock import patch

import pytest

from add_ticket_hook import main

COMMIT_CONTENT = """Summary of changes

This is some more detail about the commit
it goes on for a few lines...
"""


def run_cmd(*args: str) -> tuple[bytes | None, bytes | None, int]:
    try:
        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except OSError as e:  # pragma: no cover
        assert False, f"Failed to run `{' '.join(args)}`: {e}"

    stdout, stderr = proc.communicate()

    returncode = proc.returncode
    if proc.returncode != 0:  # pragma: no cover
        stdout_text = str(stdout) if stdout is not None else ""
        stderr_text = str(stderr) if stderr is not None else ""
        assert False, f"`{' '.join(args)}` failed: {stdout_text}, {stderr_text}"
    else:
        return stdout, stderr, returncode


@contextmanager
def using_branch(branch: str) -> Generator[None, None, None]:
    orig_branch, _, _ = run_cmd("git", "rev-parse", "--abbrev-ref", "HEAD")
    assert orig_branch is not None

    try:
        run_cmd("git", "checkout", "-b", branch)
        yield
    finally:
        run_cmd("git", "checkout", orig_branch.decode().strip())
        run_cmd("git", "branch", "-d", branch)


@pytest.fixture()
def commit_file() -> str:
    commit_file = tempfile.NamedTemporaryFile().name
    with open(commit_file, "w") as f:
        f.write(COMMIT_CONTENT)

    return commit_file


@pytest.fixture()
def git_dir(tmpdir):
    run_cmd("git", "init", str(tmpdir)),

    with tmpdir.as_cwd():
        run_cmd(
            "git", "commit", "--allow-empty", "--message", "we need at least one commit"
        )

    return tmpdir


@pytest.mark.parametrize(
    ("branch", "commit_source", "expected_ticket_trailer"),
    (
        ("FOO-12-some-changes", "commit", "Ticket: FOO-12"),
        ("FOO-12-some-changes", "template", "Ticket: FOO-12"),
        ("FOO-12-some-changes", "message", "Ticket: FOO-12"),
        ("more-detail/FOO-12-add-things", "commit", "Ticket: FOO-12"),
        ("some-name/some-feature/FEAT-12-add-feature", "commit", "Ticket: FEAT-12"),
    ),
)
def test_add_ticket_hook_with_supported_sources_and_commit_text(
    git_dir, commit_file, branch, commit_source, expected_ticket_trailer
):
    with git_dir.as_cwd(), using_branch(branch), patch.dict(
        "os.environ", {"PRE_COMMIT_COMMIT_MSG_SOURCE": commit_source}
    ):
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == f"{COMMIT_CONTENT}\n{expected_ticket_trailer}\n"


def test_writes_ticket_after_body_but_before_comments(git_dir):
    commit_comment = (
        "# Please enter the commit message for your changes. Lines starting\n"
        + "# with '#' will be ignored, and an empty message aborts the commit."
    )
    commit_file = tempfile.NamedTemporaryFile().name

    with open(commit_file, "w") as f:
        f.write(COMMIT_CONTENT + "\n" + commit_comment)

    branch = "TICKET-123-my-branch"
    expected_content = COMMIT_CONTENT + "\n\nTicket: TICKET-123\n" + commit_comment

    with git_dir.as_cwd(), using_branch(branch), patch.dict(
        "os.environ", {"PRE_COMMIT_COMMIT_MSG_SOURCE": "message"}
    ):
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == expected_content


def test_adds_extra_line_for_subject_when_only_comments(git_dir):
    commit_content = (
        "# Please enter the commit message for your changes. Lines starting\n"
        + "# with '#' will be ignored, and an empty message aborts the commit."
    )

    commit_file = tempfile.NamedTemporaryFile().name

    with open(commit_file, "w") as f:
        f.write(commit_content)

    branch = "TICKET-123-my-branch"
    expected_content = "\nTicket: TICKET-123\n\n" + commit_content

    with git_dir.as_cwd(), using_branch(branch):
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == expected_content


@pytest.mark.parametrize("commit_source", ("merge", "squash", "unknown"))
def test_no_ticket_added_for_unsupported_source(git_dir, commit_file, commit_source):
    branch = "FOO-14-more-changes"

    with git_dir.as_cwd(), using_branch(branch), patch.dict(
        "os.environ", {"PRE_COMMIT_COMMIT_MSG_SOURCE": commit_source}
    ):
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == COMMIT_CONTENT


def test_no_ticket_added_on_branch_detection_error(git_dir, commit_file):
    branch = "FOO-14-more-changes"

    # Remove `git` from PATH
    with git_dir.as_cwd(), using_branch(branch), patch.dict("os.environ", {"PATH": ""}):
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == COMMIT_CONTENT


def test_no_ticket_added_on_branch_detection_failure(tmpdir, commit_file):
    with tmpdir.as_cwd():
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == COMMIT_CONTENT


def test_no_ticket_added_when_no_ticket_in_branch(git_dir, commit_file):
    branch = "no-ticket-here"

    with git_dir.as_cwd(), using_branch(branch):
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == COMMIT_CONTENT


def test_ticket_no_added_if_already_present(git_dir, commit_file):
    branch = "FOO-123-no-ticket-here"

    with open(commit_file, "a") as f:
        f.write("\nTicket: FOO-123\n")

    with git_dir.as_cwd(), using_branch(branch):
        assert main((str(commit_file),)) == 0

    with open(commit_file, "r") as f:
        written_content = f.read()

    assert written_content == COMMIT_CONTENT + "\nTicket: FOO-123\n"
